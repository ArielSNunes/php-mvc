<?php
namespace Core;

use Utils\StatusCodes;


class Controller
{
	public function __construct()
	{
	}
	public function getRequestMethod()
	{
		return $_SERVER['REQUEST_METHOD'];
	}
	public function loadView($viewName, $viewData = array())
	{
		extract($viewData);
		require('Views' . DIRECTORY_SEPARATOR . $viewName . '.php');
	}
	public function renderTemplate($viewName, $viewData = array())
	{
		require('Views' . DIRECTORY_SEPARATOR . 'template.php');
	}
	public function getRequestData()
	{
		switch ($this->getRequestMethod()) {
			case 'GET':
				return $_GET;
				break;
			case 'PUT':
			case 'DELETE':
				parse_str(file_get_contents('php://input'), $data);
				return (array)$data;
				break;
			case 'POST':
				$data = json_decode(file_get_contents('php://input'));
				if (is_null($data)) {
					$data = $_POST;
				}
				return (array)$data;
				break;
			default:
				return $this->returnJson(405, array('msg' => 'Método não permitido'));
				break;
		}
	}
	public function returnJson($code, $arr = array())
	{
		StatusCodes::send($code);
		header('Content-Type: application/json; charset=utf-8');
		echo (json_encode($arr));
		exit;
	}
	public function verifyIssetData($data)
	{
		return (isset($this->getRequestData()[$data])
			&& !empty($this->getRequestData()[$data]));
	}
}