<?php
namespace Controllers;

use Core\Controller;
use Utils\StatusCodes;

class AjaxController extends Controller
{
	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$this->returnJson(201, array('method' => $this->getRequestMethod()));
	}
}