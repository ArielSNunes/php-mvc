<?php
session_start();
if (file_exists('vendor/autoload.php'))
	require('vendor/autoload.php');
require('config.php');
require('routes.php');
$core = new Core\Core();
$core->run();