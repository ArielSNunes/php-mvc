<?php
namespace Utils;

class Redirect
{
	private static $listOfPages = array(
		'home' => '/home',
		'err' => '/notFound'
	);

	public static function to($page)
	{
		$location = "Location: " . BASE_URL;
		if (array_key_exists($page, self::$listOfPages))
			$location .= self::$listOfPages[$PAGE];
		else
			$location .= self::$listOfPages['err'];
		header($location);
		die();
	}
}